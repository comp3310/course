# COMP3310/COMP6331 Assessment

## Overview
The course assessment is a straight add of the following components. There is no requirement to pass any one or more components.
* Laboratory exercises: 5%
* In-class quizzes: 5%
* Assignments: 30%
* Final (lab) exam: 60%

## Laboratory exercises
There are 6 x 2 hours labs in this course. Participation in up to 5 labs will be awarded 1 mark per lab.

Lab work should be prepared beforehand and can be completed beforehand and demonstrated to the lab tutor during the lab period.

All students are strongly encouraged to attend the UDP and TCP programming labs in order to participate in the joint networking activity.

## In-class quizzes
There will be two 20 minute quizzes held during the Thursday lecture time slot in weeks 5 and 9.

Each quiz will consist of multiple choice and short answer questions.

The quizzes will be co-marked, with each student required to mark another randomly selected students quiz during the second half of the lecture. This is a fun exercise and valuable for feedback both for me and for the students.

The two quizzes are worth 2.5% each, or 5% of final assessment in total. Do NOT stress about them - the marks are simply to encourage all to participate.

## Assignments
There will be two assignments.

### Assignment 1
A written research based assignment worth 10% due 11:00am Monday 4th April (start of student break)

Assignment specification will be available by Thursday lecture in week 3 (3rd March).

### Assignment 2
A programming assignment worth 20% due 11:00am Monday 23rd May (week 13) - can be done in pairs

COMP6331 students will have additional components to complete in order to attain the full 20%

Both COMP3310 and COMP6331 students can also undertake specified optional extra components to gain greater than 20%, but the combined lab, quiz and assignment mark cannot exceed 40%.
The additional components will only be marked if the base components of the assignment have been completed and pass all testing.

For both assignments there is a 100% penalty for late submission. Extensions can only be granted in exceptional circumstances and with appropriate documentation.

## Final (lab) exam
The final major assessment item is an exam to be held in the CSIT Teaching labs during the end of semester examination period. Worth 60%. Will contain content from the laboratory exercises, the guest lectures, the main lectures, the quizzes and the second assignment.
