# COMP3310/COMP6331 Computer Networks
Semester 1, 2016

![](comp3310_logo.png)

## Staff

* Course Convenor: [Ramesh Sankaranarayana](https://cecs.anu.edu.au/people/ramesh-sankaranarayana)
* Principal Lecturer: [Bob Edwards](https://cecs.anu.edu.au/people/bob-edwards)
* Tutors: Bob & Hendra Gunadi

## Lectures
(see [ANU Timetable](http://timetabling.anu.edu.au/sws2016/) for authorative information)
* Tuesdays 13:00 - 15:00 Engineering Lecture Theatre (note: 2 hour lecture)
* Thursdays 12:00 - 13:00 Engineering Lecture Theatre

Lecture recordings will (hopefully) be available on the [Wattle](http://wattlecourses.anu.edu.au/course/view.php?id=16174) page, under Echo360

## Practicals
Use [StReaMS](https://cs.anu.edu.au/streams) to register into a lab group - available Monday morning week 1

There are six 2 hour labs which will run in even numbered weeks, starting in week 2
* Tuesdays 15:00 - 17:00 CSIT N112
* Thursdays 16:00 - 18:00 CSIT N112
* Fridays 15:00 - 17:00 CSIT N112

## Assessment
see [Assessment](Assessment.md)

## Textbooks
The recommended textbooks are:
* TCP/IP Illustrated, Volume 1: The Protocols Second Edition, Fall & Stevens ([Co-op bookshop](http://www.coop.com.au/tcp-ip-illustrated-volume-1-the-protocols-second-edition/9780321336316))
* Computer Networks, New International Edition, Tanenbaum & Wetherall ([Co-op bookshop](http://www.coop.com.au/computer-networks-new-international-edition/9781292024226))

## Links
* [COMP3310 at Programs and Courses](http://programsandcourses.anu.edu.au/course/COMP3310)
* [COMP6331 at Programs and Courses](http://programsandcourses.anu.edu.au/course/COMP6331)
* [Co-op Textbook Search](http://www.coop.com.au/textbook/search/australian-national-university/1601/comp3310)
